var rootAPIUrl = "http://api.bl.datasmiths.co.uk/api/";
var dbpediaTemplate = "http://dbpedia.org/resource/";
var blCollectionItemTemplate = "http://data.bl.uk/collection-item/";
var conceptsLocation = "concepts";
var collectionItemsLocation = "collection-items";
var collectionsLocation = "collections";
var pageStorageKey = "newPage";
var conceptGraph;

//Actions when the page loads
$(document).ready(function () {
    var pageRequired = window.location.pathname.split("/")[2];
    var conceptUri = window.location.search.split("=")[1];
    if (pageRequired == "collection-item") {
        loadItemToPage(conceptUri);
    } else if (pageRequired == "place") {
        loadLocationToPage(conceptUri);
    } else if (pageRequired == "person") {
        loadPersonToPage(conceptUri);
    } else if (pageRequired == "thing") {
        loadThingToPage(conceptUri);
    } else if (pageRequired == "collection") {
        loadCollectionToPage(conceptUri);
    }
});

//Actions when a page chosen (on the place page)
$("a#location").click(function () {
    var location = $(this).html();
    loadLocationToPage(location);
});

$(document).on("click", "#full-screen-button", function () {
    showImage(conceptGraph[0]["fullImageURL"]);
});


$(document).on("click", "#bl-full-img", function () {
    hideMe(this);
});

//Actions when a collection item is chosen (on collection item page)
$("a#col-item").click(function () {
    var item = $(this).html();
    loadItemToPage(item);
});

//Actions when a person chosen (on person page)
$("a#person-item").click(function () {
    var person = $(this).html();
    loadPersonToPage(person);
});

function loadLocationToPage(location) {
    //Display Concept
    $.get(rootAPIUrl + conceptsLocation + "?uri=" + location, function (data) {
        var graph = data["@graph"][0];
        var label = graph["label"]
        conceptGraph = graph;
        $("#location-title h1").remove();
        $("#location-title").append("<div class=\"col-sm-12\"><h1>" + label + "</h1></div>");
        $("strong#location-name").empty().append(label);
        var lat = graph["lat"];
        var lon = graph["long"];
        replaceTableElementIfDataAvailable("location-type", graph["@type"]);
        replaceTableElementIfDataAvailable("location-population", graph["populationTotal"]);
        $("td#location-lat-long").empty();
        if ((typeof(lat) != 'undefined') || (typeof(long) != 'undefined')) {
            replaceTableElementIfDataAvailable("location-lat-long", lat + "/" + lon);
            $("iframe#location-map").replaceWith("<iframe src=\"" + getEmbeddedMapLink(lat, lon, 13) + "\" id=\"location-map\" width=\"750\" height=\"480\" frameborder=\"0\" style=\"border:0\"></iframe>");
        } else {
            $("iframe#location-map").replaceWith("<iframe src=\"" + getEmbeddedMapLink(21, 28, 4) + "\" id=\"location-map\" width=\"750\" height=\"480\" frameborder=\"0\" style=\"border:0\"></iframe>");
        }
        $("a#location-thumbnail").empty();
        if (typeof(graph["thumbnail"] != 'undefined')) {
            $("a#location-thumbnail").append("<img class=\"thumbnail\" src=\"" + graph["thumbnail"] + "\"/>");
        }

        addParagraphElementIfDataAvailable("location-desc", graph["comment"]);

        //Breadcrumb Handling/Popluation
        var locationPredicates = [ "country", "isPartOf", "location", "locatedInArea", "state"];
        $("ol").empty();
        $.each(locationPredicates, function (i, item) {
            if ((typeof(graph[item]) != 'undefined')) {
                if (typeof(graph[item]["@set"]) == 'undefined') {
                    var partOfUri = graph[item]["@id"];
                    $("ol").append("<li><a href=\"place?uri=" + partOfUri + "\">" + parseLabelFromAUri(partOfUri) + "</a></li>");
                } else {
                    var id1 = graph[item]["@set"][0]["@id"];
                    $("ol").append("<li><a href=\"place?uri=" + id1 + "\">" + parseLabelFromAUri(id1) + "</a></li>");
                    var id2 = graph[item]["@set"][1]["@id"];
                    $("ol").append("<li><a href=\"place?uri=" + id2 + "\">" + parseLabelFromAUri(id2) + "</a></li>");
                }
            } else {
                return true;
            }
        });
        $("ol").append("<li><a>" + label + "</a></li>");
    });

    // Display related collection items
    // Prep page
    $("#related-items-right #item").remove();
    $("div#related-parent-items").empty();
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + location + "&association=location", function (data) {
        var graph = data["@graph"];
        if (typeof(graph) == "undefined") {
            graph = [];
        }
        $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + location + "&association=depicts", function (data) {
            var depictsGraph = data["@graph"];
            if (typeof(depictsGraph) == "undefined") {
                depictsGraph = [];
            }
            var latestGraph = $.merge(graph, depictsGraph);
            $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + location + "&association=associated", function (data) {
                var associatedGraph = data["@graph"];
                if (typeof(associatedGraph) == "undefined") {
                    associatedGraph = [];
                }
                var allItems = $.merge(associatedGraph, latestGraph);
                $.each(allItems, function (i, item) {
                    if (i <= 1) {
                        $("#related-items-right").append("<div id=\"item\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\"><div class=\"bl-thumb-text\"><h4>" + item.title + "</h4></div></img></a></div>");
                    } else {
                        $("div#related-parent-items").append("<div id=\"item\" class=\"bl-masonry-thumb3\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h5>" + item.title + "</h5></div></a></div>");
                    }
                });
                if (allItems.length == 0) {
                    //TODO - Execute location search
                }
                var container = document.querySelector("div#related-parent-items");
                var msnry;
                imagesLoaded(container, function () {
                    msnry = new Masonry(container, { "columnWidth":262.5, "itemSelector":".bl-masonry-thumb3", "gutter":30 });
                });
            });
        });
    });
}

function getEmbeddedMapLink(lat, lon, zoom) {
    var mapAPIKey = "key=AIzaSyBV_DRSLCX5swiNKASBRiXqDkZE5Ws2Lsg";
    return "https://www.google.com/maps/embed/v1/view?" + mapAPIKey + "&center=" + lat + "," + lon + "&zoom=" + zoom;
}

function loadItemToPage(item) {
    var parentAssociations;
    //Display Collection Item.
    $.get(rootAPIUrl + collectionItemsLocation + "?uri=" + item, function (data) {
        var graph = data["@graph"];
        var collection = graph[0]["inCollection"];
        conceptGraph = graph;
        //Display collection items from parent
        $("div#related-parent-items").empty();
        $("span#parent-title").empty().append(collection);
        //Get and display items from the same collection
        $.get(rootAPIUrl + collectionItemsLocation + "?collectionUri=" + collection, function (data) {
            var pCollectionGraph = data["@graph"];
            var ciRow = "";
            $.each(pCollectionGraph, function (i, item) {
                if (item["@type"] == "CollectionItem") {
                    ciRow = ciRow + "<div id=\"p-item\" class=\"bl-masonry-thumb3\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h5>" + item.title + "</h5></div></a></div>";
                }
            })
            $("div#related-parent-items").append(ciRow);
            var container = document.querySelector("div#related-parent-items");
            var msnry;
            imagesLoaded(container, function () {
                msnry = new Masonry(container, { "columnWidth":262.5, "itemSelector":".bl-masonry-thumb3", "gutter":30 });
            })
        });

        //Updated collection information on page
        if (typeof(collection) != 'undefined') {
            //Clear elements
            $("ol").empty();
            $("#col-col").empty();
            $("strong#collection-name").empty();
            //Get and display information about a collection
            $.get(rootAPIUrl + collectionsLocation + "?uri=" + collection, function (data) {
                var collectionGraph = data["@graph"];
                //Store collection Graph for later use (associations).
                $("ol").empty();
                //Breadcrumb
                if (typeof(collectionGraph[0]["parentCollection"]) != 'undefined') {
                    if (typeof(collectionGraph[0]["parentCollection"]["@set"]) == 'undefined') {
                        $("ol").empty().append("<a href=\"collection?uri=" + collectionGraph[0]["parentCollection"]["@id"] + "\">" + fetchLabel(collectionGraph, collectionGraph[0]["parentCollection"]["@id"]) + "</a> | ");
                    } else {
                        var content = "";
                        $.each(collectionGraph[0]["parentCollection"]["@set"], function (i, item) {
                            var id = collectionGraph[0]["parentCollection"]["@set"][i]["@id"];
                            if (i != collectionGraph[0]["parentCollection"]["@set"].length) {
                                content = content + "<a href=\"collection?uri=" + id + "\">" + fetchLabel(collectionGraph, id) + "</a> | ";
                            } else {
                                content = content + "<a href=\"collection?uri=" + id + "\">" + fetchLabel(collectionGraph, id) + "</a>";
                            }
                        });
                        $("ol").append(content);
                    }
                }
                $("ol").append("<li><a href=\"collection?uri=" + collection + "\">" + collectionGraph[0].title + "</a></li>");
                $("strong#collection-name").empty().append("<a href=\"collection?uri=" + collection + "\">" + collectionGraph[0].title + "</a>");
                $("td#col-col").empty().append("<a href=\"collection?uri=" + collection + "\">" + collectionGraph[0].title + "</a>");
            });
        }

        //Display titles
        $("#col-titles h1").remove();
        $("#col-titles h2").remove();
        $("h4#col-title").empty().append(graph[0]["title"]);
        $("h6#col-desc").empty().append(graph[0]["subTitle"]);
        $("img#full-image").attr('src', graph[0]["fullImageURL"]);
        $("#col-titles").append("<h1>" + graph[0]["title"] + "</h1>");
        if (typeof(graph[0]["subTitle"]) != 'undefined') {
            $("#col-titles").append("<h2>" + graph[0]["subTitle"] + "</h2>");
        }

        //Display main image
        $("#item-figure img").remove();
        $("#item-figure").append("<img id=\"largeImg\" style=\"cursor:pointer\" src=\"" + graph[0]["fullImageURL"] + "\"></img>");

        //Display Metadata
        replaceTableElementIfDataAvailable("col-title", graph[0]["title"]);
        if (typeof(graph[0]["creator"]) != 'undefined') {
            if (typeof(graph[0]["creator"]["@set"]) == 'undefined') {
                $("#col-creator").empty();
                var creatorUri = graph[0]["creator"]["@id"];
                addTableElementIfDataAvailableWithLink("col-creator", creatorUri, "person");
            } else {
                $("#col-creator").empty();
                var creatorContent = "";
                $.each(graph[0]["creator"]["@set"], function (i, item) {
                    var id = graph[0]["creator"]["@set"][i]["@id"];
                    if (i == 0) {
                        creatorContent = creatorContent + "<a href=\"person?uri=" + id + "\">" + parseLabelFromAUri(id) + "</a> | ";
                    } else {
                        creatorContent = creatorContent + "<a href=\"person?uri=" + id + "\">" + parseLabelFromAUri(id) + "</a>";
                    }
                });
                $("#col-creator").append(creatorContent);
            }
        } else {
            $("#col-creator").empty().append("Unknown");
        }
        var locationUri = graph[0]["location"];
        addTableElementIfDataAvailableWithLink("col-location", locationUri, "place");
        replaceTableElementIfDataAvailable("col-created", graph[0]["creationDate"]);
        replaceTableElementIfDataAvailable("col-genre", graph[0]["genre"]);
        replaceTableElementIfDataAvailable("col-medium", graph[0]["medium"]);
        replaceTableElementIfDataAvailable("col-publisher", graph[0]["publisher"]);
        replaceTableElementIfDataAvailable("col-shelfmark", graph[0]["shelfmark"]);
        replaceTableElementIfDataAvailable("col-ddc", graph[0]["ddcClass"]);
        addParagraphElementIfDataAvailable("col-desc", graph[0]["objectDescription"]);

        //Load measurements
        if ((undefined == graph[0]["objectWidth"]) || (undefined == graph[0]["objectLength"]) || (undefined == graph[0]["unitOfMeasurement"])) {
            $("td#col-measurement").empty().append("Unknown");
        } else {
            $("td#col-measurement").empty();
            $("td#col-measurement").append("<td>" + graph[0]["objectWidth"] + " by " + graph[0]["objectLength"] + " (" + graph[0]["unitOfMeasurement"] + ")</td>");
        }

        //Display associations/depictions
        $("p#col-depicts").empty();
        if (typeof(graph[0]["depictsThing"]) != 'undefined') {
            if (typeof(graph[0]["depictsThing"]["@set"]) == 'undefined') {
                var depictionUri = graph[0]["depictsThing"]["@id"];
                addParagraphElementIfDataAvailableWithTemplate("col-depicts", depictionUri, "This item depicts ");
                appendCorrectLinkForUri("p#col-depicts", depictionUri);
            } else {
                $("p#col-depicts").append("This item depicts ");
                var trueLength = graph[0]["depictsThing"]["@set"].length - 1;
                $.each(graph[0]["depictsThing"]["@set"], function (i, item) {
                    appendCorrectLinkForUri("p#col-depicts", item["@id"]);
                    if (i < trueLength) {
                        $("p#col-depicts").append(" | ");
                    }
                });
            }
        } else {
            $("p#col-depicts").append("This item has no depictions.");
        }

        var existingAssociation = false;
        $("p#col-association").empty();
        if (typeof(graph[0]["associatesThing"]) != 'undefined') {
            if (typeof(graph[0]["associatesThing"]["@set"]) == 'undefined') {
                existingAssociation = true;
                var association = graph[0]["associatesThing"]["@id"];
                addParagraphElementIfDataAvailableWithTemplate("col-association", association, "This item is associated with ");
                appendCorrectLinkForUri("p#col-association", association);
            } else {
                existingAssociation = true;
                $("p#col-association").append("This item is associated with ");
                var trueLength = graph[0]["associatesThing"]["@set"].length - 1;
                $.each(graph[0]["associatesThing"]["@set"], function (i, item) {
                    appendCorrectLinkForUri("p#col-association", item["@id"]);
                    if (i < trueLength) {
                        $("p#col-association").append(" | ");
                    }
                });
            }
        } else {
            $("p#col-association").append("This item has no associations.");
        }
    });
}

/**
 * @param elementID
 * @param dataKey
 */
function replaceTableElementIfDataAvailable(elementID, data) {
    if (typeof(data) != 'undefined') {
        $("td#" + elementID).empty().append(data);
    } else {
        $("td#" + elementID).empty().append("Unknown");
    }
}

/**
 * @param elementID
 * @param dataKey
 */
function replaceTableElementIfDataAvailableWithLocationLink(elementID, data) {
    if (undefined != data) {
        $("td#" + elementID).empty().append("<a href=\"place?uri=" + data + "\">" + parseLabelFromAUri(data) + "</a>");
    } else {
        $("td#" + elementID).empty();
    }
}

/**
 * @param elementID
 * @param dataKey
 */
function addTableElementIfDataAvailableWithLink(elementID, data, anchorID) {
    if (undefined != data) {
        $("td#" + elementID).empty().append("<a href=\"" + anchorID + "?uri=" + data + "\">" + parseLabelFromAUri(data) + "</a>");
    } else {
        $("td#" + elementID).empty().append("Unknown");
    }
}

/**
 * @param elementID
 * @param dataKey
 */
function addParagraphElementIfDataAvailable(elementID, data) {
    if (undefined != data) {
        $("p#" + elementID).empty().append(data);
    } else {
        $("p#" + elementID).empty().append("Unknown");
    }
}

/**
 * @param elementID
 * @param dataKey
 */
function addParagraphElementIfDataAvailableWithTemplate(elementID, data, template) {
    if (undefined != data) {
        $("p#" + elementID).empty().append(template);
    } else {
        $("p#" + elementID).empty();
    }
}

/**
 * @param person
 */
function loadPersonToPage(person) {
    //Display concept data
    $.get(rootAPIUrl + conceptsLocation + "?uri=" + person, function (data) {
        var person = data["@graph"][0];
        conceptGraph = person;
        //Add person name
        $("h1#person-name").empty().append(person.label);
        $("strong#person-name").empty().append(person.label);
        addParagraphElementIfDataAvailable("person-desc", person.comment);
        $("p#person-info").empty();
        replaceTableElementIfDataAvailable("person-thumbnail", person.thumbnail);
        replaceTableElementIfDataAvailable("person-death-year", person.deathYear);
        replaceTableElementIfDataAvailable("person-birth-year", person.birthDate);
        replaceTableElementIfDataAvailable("person-birth-date", person.birthYear);
        replaceTableElementIfDataAvailableWithLocationLink("person-pob", person.birthPlace);
        replaceTableElementIfDataAvailableWithLocationLink("person-pod", person.deathPlace);
    });

    //Display depicted collection items
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + person + "&association=depicts", function (data) {
        var graph = data["@graph"];
        //Remove all current items
        $("#person-depicts-items #item").remove();
        if (typeof(graph) == 'undefined') {
            $("#depicted-items").hide();
        } else {
            $("#depicted-items").show();
            $.each(graph, function (i, item) {
                if (i <= 2) {
                    $("#person-depicts-items").append("<div id=\"item\"><a href=\"collection-item?uri=" + item["@id"] + "\" class='thumbnail bl-thumbnail'><img src=\"" + item.standardImageURL + "\" /><div class='bl-thumb-text'><h4>" + item.title + "</h4></div></a></div>");
                }
            })
        }
    });

    //Display created collection items
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + person + "&association=creator", function (data) {
        var graph = data["@graph"];
        //Remove all current items
        $("div#related-created-items").empty();
        //Hide if no creator items
        if (typeof(graph) == 'undefined') {
            $("#created-items").hide();
        } else {
            $("created-items").show();
            $.each(graph, function (i, item) {
                $("div#related-created-items").append("<div id=\"p-item\" class=\"bl-masonry-thumb4\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class='bl-thumb-text'><h4>" + item.title + "</h4></div></a></div>");
            })
            //Masonry
            var container = document.querySelector("div#related-created-items");
            var msnry;
            imagesLoaded(container, function () {
                msnry = new Masonry(container, { "columnWidth":360, "itemSelector":".bl-masonry-thumb4", "gutter":30 });
            })
        }
    });

    //Display associated collection items
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + person + "&association=associated", function (data) {
        var graph = data["@graph"];
        //Remove all current items
        $("#related-associated-items").empty();
        //Hide section if no associated items
        if (typeof(graph) == 'undefined') {
            $("#associated-items").hide();
        } else {
            $("#associated-items").show();
            $.each(graph, function (i, item) {
                $("#related-associated-items").append("<div class=\"bl-masonry-thumb3\" id=\"p-item\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class='bl-thumb-text'><h5>" + item.title + "</h5></div></a></div>");
            })
        }
        //Masonry
        var container = document.querySelector("#related-associated-items");
        var msnry;
        imagesLoaded(container, function () {
            msnry = new Masonry(container, { "columnWidth":262.5, "itemSelector":".bl-masonry-thumb3", "gutter":30 });
        })
    });
}

/**
 * @param person
 */
function loadThingToPage(thing) {
    //Display concept data
    $.get(rootAPIUrl + conceptsLocation + "?uri=" + thing, function (data) {
        var thing = data["@graph"][0];
        conceptGraph = thing;
        //Add person name
        $("h1#thing-name").empty().append(thing.label);
        $("strong#thing-name").empty().append(thing.label);
        replaceTableElementIfDataAvailable("thing-comment", thing.comment);
        $("img#thing-thumbnail").attr("src", thing.thumbnail);
    });

    //Display depicted collection items
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + thing + "&association=depicts", function (data) {
        var graph = data["@graph"];
        //Remove all current items
        $("#thing-depicts-items #item").remove();
        if (typeof(graph) == 'undefined') {
            $("#depicted-items").hide();
        } else {
            $("#depicted-items").show();
            $.each(graph, function (i, item) {
                if (i <= 1) {
                    $("#thing-depicts-items")
                        .append("<div id=\"item\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h4>" + item.title + "</h4></div></a></div>");
                }
            })
        }
    });

    //Display associated collection items
    $.get(rootAPIUrl + collectionItemsLocation + "?conceptUri=" + thing + "&association=associated", function (data) {
        var graph = data["@graph"];
        //Remove all current items
        $("#thing-associated-items").empty();
        //Hide section if no associated items
        if (typeof(graph) == 'undefined') {
            $("#associated-items").hide();
        } else {
            $("#associated-items").show();
            $.each(graph, function (i, item) {
                $("#thing-associated-items")
                    .append("<div class=\"bl-masonry-thumb3\" id=\"item\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h5>" + item.title + "</h5></div></a></div>");
            })
            //Initialise masonry
            var container = document.querySelector("div#thing-associated-items");
            var msnry;
            imagesLoaded(container, function () {
                msnry = new Masonry(container, { "columnWidth":262.5, "itemSelector":".bl-masonry-thumb3", "gutter":30 });
            })
        }
    });
}
/**
 * @param collectionUri
 */
function loadCollectionToPage(collectionUri) {
    //Display concept data
    $.get(rootAPIUrl + collectionsLocation + "?uri=" + collectionUri, function (data) {
        conceptGraph = data;
        var collectionGraph = data["@graph"][0];
        var allGraph = data["@graph"];
        //Breadcrumb
        $("ol").empty();
        //Breadcrumb
        if (typeof(collectionGraph["parentCollection"]) != 'undefined') {
            if (typeof(collectionGraph["parentCollection"]["@set"]) == 'undefined') {
                $("ol").empty().append("<a href=\"collection?uri=" + collectionGraph["parentCollection"]["@id"] + "\">" + fetchLabel(allGraph, collectionGraph["parentCollection"]["@id"]) + "</a> | ");
            } else {
                var content = "";
                $.each(collectionGraph["parentCollection"]["@set"], function (i, item) {
                    var id = collectionGraph["parentCollection"]["@set"][i]["@id"];
                    if (i != collectionGraph["parentCollection"]["@set"].length) {
                        content = content + "<a href=\"collection?uri=" + id + "\">" + fetchLabel(allGraph, id) + "</a> | ";
                    } else {
                        content = content + "<a href=\"collection?uri=" + id + "\">" + fetchLabel(allGraph, id) + "</a>";
                    }
                });
                $("ol").append(content);
            }
        }
        $("ol").append("<li><a>" + collectionGraph.title + "</a></li>");
        //Metadata
        $("title#collection-title").empty().append(collectionGraph.title);
        $("h1#collection-title").empty().append(collectionGraph.title);
        $("strong#collection-title").empty().append(collectionGraph.title);
        replaceTableElementIfDataAvailable("collection-title", collectionGraph.title);
        replaceTableElementIfDataAvailable("collection-work", collectionGraph.works);
        replaceTableElementIfDataAvailable("collection-cat-name", collectionGraph.catalogueName);
        replaceTableElementIfDataAvailable("collection-curator", collectionGraph.curator);
        replaceTableElementIfDataAvailable("collection-contributors", collectionGraph.contributors);
        replaceTableElementIfDataAvailable("collection-contributor-type", collectionGraph.contributorTypes);
        replaceTableElementIfDataAvailable("collection-contents", collectionGraph.collectionContents);
        replaceTableElementIfDataAvailable("collection-provenance", collectionGraph.collectionProvenance);
        replaceTableElementIfDataAvailable("collection-notes", collectionGraph.notes);
        replaceTableElementIfDataAvailable("collection-subjects", collectionGraph.subjects);
        replaceTableElementIfDataAvailable("collection-cat-items", collectionGraph.catalogueItems);
        $("a#collection-cat-url").empty().append(collectionGraph.catalogueUrl);
        $("a#collection-cat-url").attr("href", collectionGraph.catalogueUrl);
        addParagraphElementIfDataAvailable("collection-desc", collectionGraph.description);
    });

    //Display collection items in collection
    $.get(rootAPIUrl + collectionItemsLocation + "?collectionUri=" + collectionUri, function (data) {
        //Get graph
        var collectionItemGraph = data["@graph"];
        $("#related-items-right #item").remove();
        $("#related-parent-items").empty();
        var sideCount = 0;
        if (typeof(collectionItemGraph) != 'undefined') {
            $.each(collectionItemGraph, function (i, item) {
                if (item["@type"] == "CollectionItem") {
                    sideCount++;
                    if (sideCount <= 2) {
                        $("#related-items-right").append("<div id=\"item\"><a class=\"thumbnail bl-thumbnail\" href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h4>" + item.title + "</h4></div></a></div>");
                    } else {
                        $("#related-parent-items").append("<div id=\"item\" class=\"bl-masonry-thumb3\"><a href=\"collection-item?uri=" + item["@id"] + "\" class=\"thumbnail bl-thumbnail\"><img src=\"" + item.standardImageURL + "\" /><div class=\"bl-thumb-text\"><h5>" + item.title + "</h5></div></a></div>");
                    }
                }
            })
            //Masonry
            var container = document.querySelector("div#related-parent-items");
            var msnry;
            imagesLoaded(container, function () {
                msnry = new Masonry(container, { "columnWidth":262.5, "itemSelector":".bl-masonry-thumb3", "gutter":30 });
            })
        }
    });
}


/**
 * Downloads JSON-LD representation of concept being viewed.
 */
$(document).on("click", "a#json-download", function () {
    saveConcept(this, localStorage.getItem(pageStorageKey) + ".json", conceptGraph);
});

function saveConcept(a, filename, content) {
    contentType = 'data:application/ld+json,';
    uriContent = contentType + encodeURIComponent(JSON.stringify(content));
    a.setAttribute('href', uriContent);
    a.setAttribute('download', filename);
}

function parseLabelFromAUri(uri) {
    label = uri.split("/")[4];
    return label.replace(/_/g, " ");
}

function showImage(imgName) {
    showLargeImagePanel();
    unselectAll();
}

function showLargeImagePanel() {
    document.getElementById('largeImg').style.visibility = 'visible';
}

function unselectAll() {
    if (document.selection) document.selection.empty();
    if (window.getSelection) window.getSelection().removeAllRanges();
}

function hideMe(obj) {
    obj.style.visibility = 'hidden';
}

function appendCorrectLinkForUri(elementToAppend, uri) {
    var locationTypes = ["Place", "_Feature", "City", "PopulatedPlace", "Settlement", "Country", "Building", "Bridge", "Infrastructure"]
    var personTypes = ["Person", "Agent"]
    var type;
    var filteredGraph = conceptGraph.filter(function (element) {
        return element["@id"] == uri;
    });
    if (filteredGraph.length > 0) {
        type = filteredGraph[0]["@type"];
    }
    if ($.inArray(type, locationTypes) != -1) {
        $(elementToAppend).append("<a href=\"place?uri=" + uri + "\">" + parseLabelFromAUri(uri) + "</a>");
    } else if ($.inArray(type, personTypes) != -1) {
        $(elementToAppend).append("<a href=\"person?uri=" + uri + "\">" + parseLabelFromAUri(uri) + "</a>");
    } else {
        $(elementToAppend).append("<a href=\"thing?uri=" + uri + "\">" + parseLabelFromAUri(uri) + "</a>");
    }
}

/**
 * Filters whole graph to a particular @ID and returns the title for the uri specified
 * @param wholeGraph
 * @param uri
 * @return {*}
 */
function fetchLabel(wholeGraph, uri) {
    var filteredGraph = wholeGraph.filter(function (element) {
        return element["@id"] == uri;
    });
    if (filteredGraph.length > 0) {
        return filteredGraph[0]["title"];
    }
}